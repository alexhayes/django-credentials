# Django Credentials

Storage and retrieval of access credentials.

## Requirements

### pycrypto

```bash
pip install pycrypto==2.6.1
```

## Installation

```bash
pip install git+https://bitbucket.org/alexhayes/django-credentials
```

## Thanks

Thanks to roi.com.au

## Author

Alex Hayes <alex@alution.com>
